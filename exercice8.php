<?php 
    /*realisation d'une bibliotheque d'images avec la possibilité 
    d'ajouter de modifier ou de supprimer une image(tout,par page ou selon les critéres)*/
    $conn = new PDO('mysql:host=localhost;dbname=galerie_photo','root','');
    if(isset($_POST['imageajoutée']))/*le name du submit*/ {
        $donneesImage = [
            'img_link' => 'images/'.$_FILES['image']['name'],
            'img_file' => $_FILES['image']['tmp_name'],
        ];
        $donnees = [
            'title' => htmlspecialchars($_POST['title']),
            'img_link' => $donneesImage['img_link']
        
        ];
        move_uploaded_file($donneesImage['img_file'],$donneesImage['img_link']);
        $sql = "INSERT INTO images(title,link) VALUES (:title,:img_link)";
        $addImage =$conn ->prepare($sql);
        $addImage->execute($donnees);
    }
    $requete = "SELECT *FROM images";
    $getDataImages = $conn->prepare($requete);
    $getDataImages->execute();
    $images = $getDataImages->fetchAll(PDO::FETCH_ASSOC);
  
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bibliotheque d'images</title> 
    <!-- <link rel="stylesheet" href="style.css"> -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
</head>
<body>

    <div class="container-fluid mx-0 px-0">
        
        <!-- entete -->
            <div class="container-fluid bg-dark text-white shadow-sm mx-0 px-0 pb-2">

            <!-- greet -->
                <div class="display-5 text-center py-5"> Bienvenue dans notre Bibliothèque d'images</div>
            <!-- fin greet -->

            <!-- form container -->

                <div class="container-fluid ajoutImages">

                    <!-- form -->

                        <form action=""method="post" enctype="multipart/form-data">

                            <div class="container w-50">

                                <!-- Choix -->
                                    <div class="form-group my-3">
                                        <label class="form-label" for="photo">Choisir Image</label>
                                        <input class="form-control" type="file" accept="image/png, image/jpeg" id="photo" name="image"> 
                                    </div>
                                <!-- Fin Choix -->
                            
                                <!-- Description -->
                                    <div class="form-group my-3">
                                        <label class="form-label" for="title">Description de la photo</label>
                                        <input class="form-control" type="text" name="title"id="title"> 
                                    </div>
                                <!-- Fin Description -->

                                <!-- Post -->
                                    <div class="container my-3 text-center">
                                        <label class="form-label" for="photo"></label>
                                        <button class="btn btn-lg w-50 btn-outline-light text-center"type="submit" name="imageajoutée">Poster</button> 
                                    </div>
                                <!-- Fin Post -->
                                
                            </div>

                        </form>

                    <!-- form -->

                </div>

            <!-- fin form container -->
                
            </div>
        <!-- fin entete -->

        <!-- affichage des images -->
            <div class="row ps-5 mx-5">
                <?php foreach($images as $image){  ?>

                    <div class="col-xxl-3">
                    <div class="card bg-light mx-5 my-5 shadow-lg" style="width:350px">
                        
                        <img class="card-img-top" src="<?php echo $image['link']; ?>" alt="<?php echo $image['title']; ?>" style="width: 100%; height:350px">

                        <div class="card-body text-center">

                            <h4 class="card-title text-center"><?=$image['title']?></h4>

                            <a class="btn btn-lg w-75 btn-danger my-2" href="delete.php?id=<?= $image['id'] ?>">
                        
                                <button class="btn text-white" type="submit">Supprimer</button>
                            </a>
                            
                        </div>

                    </div>
                    </div>

                <?php } ?>
            </div>
        <!-- Fin affichage des images -->
        
    </div>
</body>
</html>