DROP DATABASE IF EXISTS galerie_photo;
CREATE DATABASE IF NOT EXISTS galerie_photo;
GRANT ALL PRIVILEGES ON galerie_photo.* to 'root' IDENTIFIED BY '';
USE galerie_photo;
DROP TABLE IF EXISTS images;
CREATE TABLE IF NOT EXISTS images(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
	title VARCHAR(255) ,
	link VARCHAR(255) NOT NULL
);

INSERT INTO images(title,link) VALUES
('pink','images/pink.jpg'),
('love','images/love.jpg'),
('pint','images/dream.jpg'),
('car','images/love.jpg'),
('pint','images/shutterstock_474434224-min_opt-749x280.jpg');


-- create database galerie_photo;
-- use galerie_photo;
-- create table images(
-- 	id INT AUTO_INCREMENT,
-- 	title varchar(255),
-- 	link varchar(255)
	
-- );